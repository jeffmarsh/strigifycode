# Stringify Code Challenge

Welcome to my code challenge for Stringify.

Thank you for giving me the opportunity to show you samples of what I am capable of.


##Here is what I got in the email:
It'll require you to use a few node modules, but I think there are lots of examples around the 'net.  Let me know if you have any problems/questions.

 - Reads the values stored in the redis LIST titled “jeff1"
 - Redis server = codetest.millanetworks.com, port 6379, password lukdebccnclijhthtnlugjdehihnuude
 - Lists are destructive reads (if using RPOP or BRPOP), so once you read data, you will not be able to read it again.  Therefore, we have stored identical data in the list “jeff2” through “jeff50”    Feel free to use any of these to test :)
 - The value is a JSON object that contains userids and an array of zip codes for that user’s vacation homes.
Connect to the Weather Underground API using the API key 
343fae88a6936714
 - For each zip code, do an API lookup and obtain the current_observation.station_id as well as the current_observation.temp_f
 - Throttle your connections as to not make >10 per minute (API limits)
 - Send a message via  socket.io called “results” (server codetest.millanetworks.com port 80) with a JSON object:  { userid: userid, station_id: station_id, temp: temp, zip: zip}  
 - If a user has more than one zip code, you will send one message for each zip code (so you may send more than one message with the same userid).
 - The server will emit the answer to you via socket.io called "answer".   Print message to the console 

**The socket.io stuff may or may not be working - if it's not, write as much code as you can as if it were.**

## Here are my notes and descriptions

### 1. Redis Lists ###
I wasn't sure of you wanted me to use rpop or not. You say "if". If it was just me, I would use lrange to get the size of the list then iterate over it from there. I have read some pluses and minuses on lrange but it seems like if I don't need the list in any specific order, then lrange should be fine, plus it saves me from getting the list size first so I can loop over it. Anyway, I made both functions (lrange and rpop) in the code. One is just commented out.
### 2. Rate limiting ###
I went ahead and used a module that I have used in the past. I thought about making my own rate limiter, but I really like the way this specific module works. Plus, based on our talk a few days ago, you seemed more interested in talking to API's, so I concentrated on that.
### 3. Socket ###
I didn't get anything from the socket codetest.millanetworks.com. If it wasn't for the comment about "it may not be working", I would never submit this. So, what I did to test the socket.io connect was to write a quick socket based entirely off the example on socket.io's site. I am able to send data to my socket and receive something back. 
Thats the only reason I am calling this done. Let me know.