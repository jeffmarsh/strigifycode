/*
// Shamelessly stolen from http://socket.io/docs/
// I could not connect to the socket mentioned in the email
// So I stole some code from the socket.io docs site and made my ow to test
// It seems to be working
*/

var app = require('http').createServer(handler)
var io = require('socket.io')(app);

app.listen(8080);

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

io.on('connection', function (socket) {
	console.log("connection accepted");
	socket.on("results", function(data){
		console.log(data);
		socket.emit("answer", "something");
	});
});
