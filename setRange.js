/*
// this is my file to populate a local redis instance.
// it can go away, and is not realted to any of the working project
// strictly local dev stuff
*/

var redis = require("redis");

var client = redis.createClient();

client.on('connect', function() {
    console.log('connected');
});

var list=['jeff1',
	'{"userid":"christine","zipcodes":["03589","32099"]}',
  '{"userid":"sally","zipcodes":["44105","02108","95389"]}',
  '{"userid":"john","zipcodes":["99734"]}',
  '{"userid":"bob","zipcodes":["85327","95050"]}',
  '{"userid":"kris","zipcodes":["95030","95971"]}' ];

client.rpush(list, function(e,r){
	console.log(r);	
	client.end();
});


client.on("error", function (err) {
        console.log("Error " + err);
    });

//client.end();
