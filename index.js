var redis = require("redis"),
	http = require("http"),
	RateLimiter = require("limiter").RateLimiter,
	Path = require("path"),
	//socket = require("socket.io-client")("http://codetest.millanetworks.com"),
	socket = require("socket.io-client")("http://localhost:8080"),
	limiter = new RateLimiter(10, "minute");

var dbName = "jeff3";

// codetest db
var client = redis.createClient(6379, "codetest.millanetworks.com");

client.auth("lukdebccnclijhthtnlugjdehihnuude", function(){
	console.log("authed");
})

// local redis connection
//var client = redis.createClient();

var wu_info = {
	"host" : "api.wunderground.com",
	"root_path" : "/api/",
	"key" : "343fae88a6936714",
	"path" : "/conditions/q/"
};

client.on("connect", function() {
    //console.log('connected');
});

/*

 It isn't entirely clear if you wanted me to use rpop or not. You say "if".
 Normaly, I wouldn't use rpop because it's destructive.
 Plus it seems like more work to first go get the length, then pop each element.
 
 But, I decided to error on the side of caution and write both the rpop method as
 well as the lrange method (which I prefer).

*/


// lrange method

client.lrange(dbName, 0, -1, function(e, list) {
	if(e){
		console.log(err);
	} else {
		for (var a=0; a< list.length; a++){
			parseIt(list[a]);
		}
		client.end();
	}
});


// llen - rpop method
/*
client.llen(dbName, function(e, ll){
	console.log(ll);
	for (listLen = 0; listLen <= ll; listLen++){
		client.rpop(dbName, function(er, val){
			if(val == null){
				client.end();
			} else {
				parseIt(val);
			}
		});
	}
	
});
*/


client.on("error", function (err) {
        console.log("REDIS Error " + err);
});

function parseIt(data){
	//console.log("parseing",data);
        var line = JSON.parse(data);
        //console.log(line.userid);
       	 for (var zLen=0; zLen < line.zipcodes.length; zLen++){
       	   limiter.removeTokens(1, function(err, remainingRequests) {
	        call_wu(line.userid, line.zipcodes[zLen]);
           });
         }
}

function call_wu(userid, zipcode){
	var apiPath = Path.join(wu_info.root_path, wu_info.key, wu_info.path, zipcode+".json");	
	var options = {
 		 hostname: wu_info.host,
 		 port: 80,
 		 path: apiPath,
 		 method: 'GET',
 		 headers: {}
	};

	var req = http.request(options, function(res) {
  		res.setEncoding("utf8");
		var body = "";
  		res.on('data', function (chunk) {
			body += chunk;
  		});
		res.on("end", function(){
			var tmp = JSON.parse(body);
			var obj = {
				"userid": userid,
				"station_id": tmp.current_observation.station_id,
				"temp": tmp.current_observation.temp_f,
				"zip": zipcode
			};
			sendData(obj);
		});
	});

	req.on("error", function(e) {
  		console.log("Request ERROR" + JSON.stringify(e));
	});

	req.end();
}

function sendData(obj){
	console.log("sending Data");
	socket.on("connect", function () {
		//console.log("socket connected");
	});
	socket.emit("results", obj);
	socket.on("answer", function(msg){
		console.log('answer',msg);
	});
}
